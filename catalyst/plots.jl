# This file generates the plots with the results of the main program.
# Variables zc are read from the execution of file matrix.jl. 

using PyPlot

close("all")
# Plots

# Time vector

t = Array{Float64}(undef,N*M,1)
for i in 1:N
	for j in 1:M
      if i == 1
         tpre = 0
      else
         tpre = t[M*(i-1)]
      end
      t[j+M*(i-1)] = zc[j]*hs[i] + tpre
	end
end

figure()
plot(t,z1s,t,z2s,t,us,linewidth=2)
xlabel(L"t",fontsize=14)
legend([L"z_1",L"z_2",L"u"],fontsize=12)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)
savefig("catalyst.pdf")

figure()
plot(t,λ1s,t,λ2s,linewidth=2)
xlabel(L"t",fontsize=14)
legend([L"\lambda_1",L"\lambda_2"],fontsize=12)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)
savefig("adjoints.pdf")


figure()
plot(t,μ1s,t,μ2s,linewidth=2)
xlabel(L"t",fontsize=14)
legend([L"\mu_1",L"\mu_2"],fontsize=12)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)
savefig("multipliers.pdf")

# Hu
Hus = λ1s.*(k2*z2s .- k1*z1s) .+ λ2s.*(k1*z1s .- k2*z2s .+ k3*z2s) + μ1s .- μ2s

# Switching function
σ = λ1s.*(k2*z2s .- k1*z1s) .+ λ2s.*(k1*z1s .- k2*z2s .+ k3*z2s) 

figure()
plot(t,Hs,t,Hus,linewidth=2)
xlabel(L"t",fontsize=14)
legend([L"H",L"H_u"],fontsize=12)
axis([0,t[end],-1e-3,1e-3])
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)
savefig("Hamiltonian.pdf")

figure()
plot(t,σ,linewidth=2)
xlabel(L"t",fontsize=14)
legend([L"\sigma"],fontsize=12)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)
savefig("SwitchingF.pdf")
