  
# Size parameters
N = 30;    # Number of finite elements
M = 7;     # Number of collocation points (including both ends)

# Sets
ni = 1:N;
mj = 1:M;
mk = copy(mj);
mj2 = 2:M

p = 4 # Inner point for spline interp
 
# Scalars
z1in = 1   # Initial value for z1
z2in = 0   # Initial value for z2
z3in = 0   # Initial value for z3
z3f  = 4   # Final value for z3
λ1f  = 1   # Final value for λ1
λ2f  = 1   # Final value for λ2
k1  = 1   # Constant k1
k2  = 10  # Constant k2
k3  = 1   # Constant k3
uL  = 0   # Lower bound for u
uU  = 1   # Upper bound for u
ρ    = 100  # Penalty parameter

# Collocation matrices
include("matrix.jl")       # It provides the collocation matrx and zeros
A,zc = colmatrix(M,"Radau");

# Weights associated with Radau quadrature
include("tableau.jl")    
Tab,ws,rs = butcher(M,"Radau");
w = Array{Float64}(undef,M)
w[2:M] = ws


