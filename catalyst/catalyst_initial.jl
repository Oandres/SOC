# This Julia file belongs to the supplementary material of the paper:
#
# "An indirect approach for singular optimal control problems" 
#  - Oswaldo Andrés-Martínez, Lorenz T. Biegler, Antonio Flores-Tlacuahuac
#
# catalyst_initial.jl generates the initial guess for the Catalyst Mixing problem
# by applying a simultaneous direct transcription method
#
#  See readme.1st for more information


using JuMP, Ipopt, PyPlot, DelimitedFiles

let
   # Model definition
   solver = Ipopt.Optimizer
   m = Model(solver)

   include("param.jl")

   # Variables
   @variable(m,z1[i in ni,j in mj],start = z1in)
   @variable(m,z2[i in ni,j in mj],start = z2in)
   @variable(m,z3[i in ni,j in mj],start = z3in)
   @variable(m,uL <= u[i in ni,j in mj] <= uU)
   @variable(m,0.00001 <= h[i in ni]<= 4.2/N,start = 4/N) 

   # Expressions for spline interpolation
   @NLexpression(m,Δup[i in ni],u[i,M]-u[i,p])
   @NLexpression(m,Δu0[i in ni],u[i,p]-u[i,1])
   @NLexpression(m,Δτp,zc[end]-zc[p])
   @NLexpression(m,Δτ0,zc[p]-zc[1])
   @NLexpression(m,Cp[i in ni],3*(Δup[i]/Δτp - Δu0[i]/Δτ0)/(2*(Δτ0+Δτp)))
   @NLexpression(m,b0[i in ni],Δu0[i]/Δτ0 - Δτ0*Cp[i]/3)
   @NLexpression(m,bp[i in ni],Δup[i]/Δτp - 2*Δτp*Cp[i]/3)
   @NLexpression(m,d0[i in ni],Cp[i]/(3*Δτ0))
   @NLexpression(m,dp[i in ni],-Cp[i]/(3*Δτp))

   # Objective function
   @NLobjective(m,Min,z1[N,M] + z2[N,M] - 1)

   # Constraints
   # - States equations
   @NLconstraint(m,g1[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*z1[i,k] for k in mk) == 
                                                     u[i,j]*(k2*z2[i,j]-k1*z1[i,j]))
   @NLconstraint(m,g2[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*z2[i,k] for k in mk) == 
                             u[i,j]*(k1*z1[i,j]-k2*z2[i,j]) - (1-u[i,j])*k3*z2[i,j])

   @NLconstraint(m,g3[i in ni,j in mj2],
                                      (1/h[i])*sum(A[j,k]*z3[i,k] for k in mk) == 1) 
      
   # - Continuity condition on state variables
   @NLconstraint(m,g4[i in 2:N],z1[i-1,M] == z1[i,1])
   @NLconstraint(m,g5[i in 2:N],z2[i-1,M] == z2[i,1])
   @NLconstraint(m,g6[i in 2:N],z3[i-1,M] == z3[i,1])

   # - Cubic spline interpolation for u
   @NLconstraint(m,g7a[i in ni,j in 2:(p-1)],u[i,j] ==
                      u[i,1] + b0[i]*(zc[j]-zc[1]) + d0[i]*(zc[j]-zc[1])^3)

   @NLconstraint(m,g7b[i in ni,j in (p+1):(M-1)],u[i,j] == u[i,p] 
             + bp[i]*(zc[j]-zc[p]) + Cp[i]*(zc[j]-zc[p])^2 + dp[i]*(zc[j]-zc[p])^3)

   # - Boundary values 
   @NLconstraint(m,g8,z1[1,1] == z1in)
   @NLconstraint(m,g9,z2[1,1] == z2in)
   @NLconstraint(m,g10,z3[1,1] == z3in)
   @NLconstraint(m,g11,z3[N,M] == z3f)

   # Solution
   optimize!(m)

   # Reading results
   z1s = transpose(convert(Array,value.(z1[:,:])))[:]
   z2s = transpose(convert(Array,value.(z2[:,:])))[:]
   z3s = transpose(convert(Array,value.(z3[:,:])))[:]
   us = transpose(convert(Array,value.(u[:,:])))[:]
   hs = convert(Array,value.(h[:]))

   # - Duals
   λ1s = Array{Float64}(undef,N,M)
   λ2s = Array{Float64}(undef,N,M)
   λ3s = Array{Float64}(undef,N,M)

   for i in ni
      for j in mj2
         λ1s[i,j] = dual(g1[i,j])/(hs[i]*w[j])
         λ2s[i,j] = dual(g2[i,j])/(hs[i]*w[j])
         λ3s[i,j] = dual(g3[i,j])/(hs[i]*w[j])
      end
   end



   λ1s[2:N,1] = λ1s[1:(N-1),M] 
   λ2s[2:N,1] = λ2s[1:(N-1),M] 
   λ3s[2:N,1] = λ3s[1:(N-1),M] 

   λ1s = transpose(λ1s)[:]
   λ2s = transpose(λ2s)[:]
   λ3s = transpose(λ3s)[:]

   # Exporting results 
   open("initialx.txt","w") do xin
      for i in 1:N*M
         writedlm(xin,[z1s[i] z2s[i] z3s[i]])
      end
   end

   open("initialh.txt","w") do hin
      for i in 1:N
         writedlm(hin,hs[i])
      end
   end

   open("initialu.txt","w") do uin
      for i in 1:N*M
         writedlm(uin,us[i])
      end
   end

   open("initiald.txt","w") do λin
      for i in 1:N*M
         writedlm(λin,[λ1s[i] λ2s[i] λ3s[i]])
      end
   end

   # Time vector
   t = Array{Float64}(undef,N*M,1)
   for i in 1:N
	   for j in 1:M
         if i == 1
            tpre = 0
         else
            tpre = t[M*(i-1)]
         end
         t[j+M*(i-1)] = zc[j]*hs[i] + tpre
	   end
   end

   # Plotting
   figure()
   plot(t,z1s,t,z2s,linewidth=2)
   xlabel(L"t",fontsize=14)
   legend([L"z_1",L"z_2"],fontsize=12)
   ax = gca()
   setp(ax[:get_yticklabels](),fontsize=12)
   setp(ax[:get_xticklabels](),fontsize=12)
   savefig("states_initial.pdf")

   figure()
   plot(t,us,linewidth=2)
   xlabel(L"t",fontsize=14)
   legend([L"u"],fontsize=12)
   axis([0,t[end],uL,uU])
   ax = gca()
   setp(ax[:get_yticklabels](),fontsize=12)
   setp(ax[:get_xticklabels](),fontsize=12)
   savefig("control_initial.pdf")
end



