# Initial guess previously obtained 
Zin = readdlm("initialx.txt",'\t')
uin = readdlm("initialu.txt",'\t')
λin = readdlm("initiald.txt",'\t')
z10 = Zin[:,1]
z20 = Zin[:,2]
z30 = Zin[:,3]
z40 = Zin[:,4]
z50 = Zin[:,5]
z60 = Zin[:,6]
z70 = Zin[:,7]
z80 = Zin[:,8]
u10 = uin[:,1]
u20 = uin[:,2]
λ10 = λin[:,1]
λ20 = λin[:,2]
λ30 = λin[:,3]
λ40 = λin[:,4]
λ50 = λin[:,5]
λ60 = λin[:,6]
λ70 = λin[:,7]

# Set initial guess
for i in ni
   for j in mj
      set_start_value(z1[i,j],z10[j+M*(i-1)])
   end
end

for i in ni
   for j in mj
      set_start_value(z2[i,j],z20[j+M*(i-1)])
   end
end

for i in ni
   for j in mj
      set_start_value(z3[i,j],z30[j+M*(i-1)])
   end
end
for i in ni
   for j in mj
      set_start_value(z4[i,j],z40[j+M*(i-1)])
   end
end

for i in ni
   for j in mj
      set_start_value(z5[i,j],z50[j+M*(i-1)])
   end
end

for i in ni
   for j in mj
      set_start_value(z6[i,j],z60[j+M*(i-1)])
   end
end

for i in ni
   for j in mj
      set_start_value(z7[i,j],z70[j+M*(i-1)])
   end
end

for i in ni
   for j in mj
      set_start_value(z8[i,j],z80[j+M*(i-1)])
   end
end

for i in ni
   for j in mj
      set_start_value(u1[i,j],u10[j+M*(i-1)])
   end
end

for i in ni
   for j in mj
      set_start_value(u2[i,j],u20[j+M*(i-1)])
   end
end

for i in ni
   for j in mj
      set_start_value(λ1[i,j],λ10[j+M*(i-1)])
   end
end

for i in ni
   for j in mj
      set_start_value(λ2[i,j],λ20[j+M*(i-1)])
   end
end

for i in ni
   for j in mj
      set_start_value(λ3[i,j],λ30[j+M*(i-1)])
   end
end

for i in ni
   for j in mj
      set_start_value(λ4[i,j],λ40[j+M*(i-1)])
   end
end

for i in ni
   for j in mj
      set_start_value(λ5[i,j],λ50[j+M*(i-1)])
   end
end

for i in ni
   for j in mj
      set_start_value(λ6[i,j],λ60[j+M*(i-1)])
   end
end

for i in ni
   for j in mj
      set_start_value(λ7[i,j],λ70[j+M*(i-1)])
   end
end

