# This Julia file belongs to the supplementary material of the paper:
#
# "An indirect approach for singular optimal control problems" 
#  - Oswaldo Andrés-Martínez, Lorenz T. Biegler, Antonio Flores-Tlacuahuac
#
# LeeRamirez.jl solves the Lee-Ramirez problem by applying an indirect approach
#
# See readme.md for more information

using JuMP, Ipopt, PyPlot, DelimitedFiles, LinearAlgebra

# Model definition
solver = Ipopt.Optimizer
m = Model(solver)

include("param.jl")

# Variables
@variable(m,z1[i in ni,j in mj])
@variable(m,z2[i in ni,j in mj])
@variable(m,z3[i in ni,j in mj])
@variable(m,z4[i in ni,j in mj])
@variable(m,z5[i in ni,j in mj])
@variable(m,z6[i in ni,j in mj])
@variable(m,z7[i in ni,j in mj])
@variable(m,z8[i in ni,j in mj])
@variable(m,λ1[i in ni,j in mj])
@variable(m,λ2[i in ni,j in mj])
@variable(m,λ3[i in ni,j in mj])
@variable(m,λ4[i in ni,j in mj])
@variable(m,λ5[i in ni,j in mj])
@variable(m,λ6[i in ni,j in mj])
@variable(m,λ7[i in ni,j in mj])
@variable(m,λ8[i in ni,j in mj])
@variable(m,Ham[i in ni,j in mj],start=3.58)
@variable(m,μ11[i in ni,j in mj]>=0)
@variable(m,μ12[i in ni,j in mj]>=0)
@variable(m,μ21[i in ni,j in mj]>=0)
@variable(m,μ22[i in ni,j in mj]>=0)
@variable(m,uL <= u1[i in ni,j in mj] <= uU)
@variable(m,uL <= u2[i in ni,j in mj] <= uU)
@variable(m,0.01 <= h[i in ni]<= 11.7/N,start=10/N)

include("setinitial.jl")

# Auxiliary expressions
@NLexpression(m,d1[i in ni,j in mj],14.35 + z3[i,j] + z3[i,j]^2/111.5)
@NLexpression(m,d2[i in ni,j in mj],0.22 + z5[i,j])
@NLexpression(m,d3[i in ni,j in mj],0.22*z7[i,j]/d2[i,j] + z6[i,j])
@NLexpression(m,f1[i in ni,j in mj],z3[i,j]*d3[i,j]/d1[i,j])
@NLexpression(m,f2[i in ni,j in mj],
                              0.233*z3[i,j]*(0.0005+z5[i,j])/(0.022+z5[i,j])/d1[i,j])
@NLexpression(m,f3[i in ni,j in mj],0.09*z5[i,j]/(0.034+z5[i,j]))

@NLexpression(m,dd1dz3[i in ni,j in mj],1 + 2*z3[i,j]/111.5)
@NLexpression(m,dd2dz5[i in ni,j in mj],1)
@NLexpression(m,dd3dz5[i in ni,j in mj],-0.22*z7[i,j]*dd2dz5[i,j]/d2[i,j]^2)
@NLexpression(m,dd3dz6[i in ni,j in mj],1)
@NLexpression(m,dd3dz7[i in ni,j in mj],0.22/d2[i,j])
@NLexpression(m,df1dz3[i in ni,j in mj],d3[i,j]*(-z3[i,j]*dd1dz3[i,j]/d1[i,j]^2
                                                        + 1/d1[i,j]))
@NLexpression(m,df1dz5[i in ni,j in mj],z3[i,j]*dd3dz5[i,j]/d1[i,j])
@NLexpression(m,df1dz6[i in ni,j in mj],z3[i,j]*dd3dz6[i,j]/d1[i,j])
@NLexpression(m,df1dz7[i in ni,j in mj],z3[i,j]*dd3dz7[i,j]/d1[i,j])
@NLexpression(m,df2dz3[i in ni,j in mj],
(0.233*(0.0005+z5[i,j])/(0.022+z5[i,j]))*(1/d1[i,j] - z3[i,j]*dd1dz3[i,j]/d1[i,j]^2))

@NLexpression(m,df2dz5[i in ni,j in mj],0.0050095*z3[i,j]/d1[i,j]/(0.022+z5[i,j])^2)
@NLexpression(m,df3dz5[i in ni,j in mj],0.00306/(0.034+z5[i,j])^2)


# Objective function 
@NLobjective(m,Min,ρ*sum(μ11[i,j]*(1-u1[i,j]) for i in ni,j in mj)
                    + ρ*sum(μ21[i,j]*(1-u2[i,j]) for i in ni,j in mj)  
                    + ρ*sum(μ12[i,j]*(u1[i,j]) for i in ni,j in mj)
                    + ρ*sum(μ22[i,j]*(u2[i,j]) for i in ni,j in mj))

# Constraints 
# - State equations
@NLconstraint(m,g1[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*z1[i,k] for k in mk) 
                                                                  == u1[i,j]+u2[i,j])

@NLconstraint(m,g2[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*z2[i,k] for k in mk) 
                              == f1[i,j]*z2[i,j] - (u1[i,j]+u2[i,j])*z2[i,j]/z1[i,j])

@NLconstraint(m,g3[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*z3[i,k] for k in mk)
      == c1*u1[i,j]/z1[i,j] - (u1[i,j]+u2[i,j])*z3[i,j]/z1[i,j] - f1[i,j]*z2[i,j]/c2)

@NLconstraint(m,g4[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*z4[i,k] for k in mk) 
                              == f2[i,j]*z2[i,j] - (u1[i,j]+u2[i,j])*z4[i,j]/z1[i,j])

@NLconstraint(m,g5[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*z5[i,k] for k in mk) 
                           == c3*u2[i,j]/z1[i,j] - (u1[i,j]+u2[i,j])*z5[i,j]/z1[i,j])
@NLconstraint(m,g6[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*z6[i,k] for k in mk) 
                                                                 == -f3[i,j]*z6[i,j])
@NLconstraint(m,g7[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*z7[i,k] for k in mk)
                                                              == f3[i,j]*(1-z7[i,j]))
   
@NLconstraint(m,g8[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*z8[i,k] for k in mk) == 1)
       
# - Adjoint equations
@NLconstraint(m,g9[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*λ1[i,k] for k in mk) ==
        - λ2[i,j]*(u1[i,j]+u2[i,j])*z2[i,j]/z1[i,j]^2 + λ3[i,j]*c1*u1[i,j]/z1[i,j]^2
        - λ3[i,j]*(u1[i,j]+u2[i,j])*z3[i,j]/z1[i,j]^2 
        - λ4[i,j]*(u1[i,j]+u2[i,j])*z4[i,j]/z1[i,j]^2 + λ5[i,j]*c3*u2[i,j]/z1[i,j]^2
        - λ5[i,j]*(u1[i,j]+u2[i,j])*z5[i,j]/z1[i,j]^2)
@NLconstraint(m,g10[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*λ2[i,k] for k in mk) == 
        - λ2[i,j]*f1[i,j] + λ2[i,j]*(u1[i,j]+u2[i,j])/z1[i,j] + λ3[i,j]*f1[i,j]/c2
        - λ4[i,j]*f2[i,j])
@NLconstraint(m,g11[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*λ3[i,k] for k in mk) == 
        - λ2[i,j]*z2[i,j]*df1dz3[i,j] + λ3[i,j]*(u1[i,j]+u2[i,j])/z1[i,j] 
        + λ3[i,j]*z2[i,j]*df1dz3[i,j]/c2 - λ4[i,j]*z2[i,j]*df2dz3[i,j])

@NLconstraint(m,g12[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*λ4[i,k] for k in mk) ==
          λ4[i,j]*(u1[i,j]+u2[i,j])/z1[i,j])

@NLconstraint(m,g13[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*λ5[i,k] for k in mk) ==
        - λ2[i,j]*z2[i,j]*df1dz5[i,j] + λ3[i,j]*z2[i,j]*df1dz5[i,j]/c2
        - λ4[i,j]*z2[i,j]*df2dz5[i,j] + λ5[i,j]*(u1[i,j]+u2[i,j])/z1[i,j]
        + λ6[i,j]*z6[i,j]*df3dz5[i,j] - λ7[i,j]*(1-z7[i,j])*df3dz5[i,j])

@NLconstraint(m,g14[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*λ6[i,k] for k in mk) ==
        - λ2[i,j]*z2[i,j]*df1dz6[i,j] + λ3[i,j]*z2[i,j]*df1dz6[i,j]/c2 
        + λ6[i,j]*f3[i,j])

@NLconstraint(m,g15[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*λ7[i,k] for k in mk) ==
        - λ2[i,j]*z2[i,j]*df1dz7[i,j] + λ3[i,j]*z2[i,j]*df1dz7[i,j]/c2
        + λ7[i,j]*f3[i,j])

@NLconstraint(m,g16[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*λ8[i,k] for k in mk) == 0)

# - Expression for the Hamiltonian
@NLexpression(m,ham[i in ni,j in mj],Ham[i,j] == λ1[i,j]*(u1[i,j]+u2[i,j]) 
   + λ2[i,j]*f1[i,j]*z2[i,j] - λ2[i,j]*(u1[i,j]+u2[i,j])*z2[i,j]/z1[i,j]
   + λ3[i,j]*c1*u1[i,j]/z1[i,j] - λ3[i,j]*(u1[i,j]+u2[i,j])*z3[i,j]/z1[i,j] 
   - λ3[i,j]*f1[i,j]*z2[i,j]/c2 
   + λ4[i,j]*f2[i,j]*z2[i,j] - λ4[i,j]*(u1[i,j]+u2[i,j])*z4[i,j]/z1[i,j] 
   + λ5[i,j]*c3*u2[i,j]/z1[i,j] - λ5[i,j]*(u1[i,j]+u2[i,j])*z5[i,j]/z1[i,j]
   - λ6[i,j]*f3[i,j]*z6[i,j] + λ7[i,j]*f3[i,j]*(1-z7[i,j]) + λ8[i,j]
   + μ11[i,j]*(u1[i,j]-1) - μ12[i,j]*u1[i,j] 
   + μ21[i,j]*(u2[i,j]-1) - μ22[i,j]*u2[i,j])


# - dH/du = 0
@NLconstraint(m,g17[i in ni,j in mj],λ1[i,j] - (λ2[i,j]*z2[i,j] - λ3[i,j]*c1 
        + λ3[i,j]*z3[i,j] + λ4[i,j]*z4[i,j] + λ5[i,j]*z5[i,j])/z1[i,j]
        + μ11[i,j] - μ12[i,j] == 0)

@NLconstraint(m,g18[i in ni,j in mj],λ1[i,j] - (λ2[i,j]*z2[i,j] + λ3[i,j]*z3[i,j] 
        + λ4[i,j]*z4[i,j] - λ5[i,j]*c3 + λ5[i,j]*z5[i,j])/z1[i,j]
        + μ21[i,j] - μ22[i,j] == 0)

# - Continuity conditions
@NLconstraint(m,g19[i in 2:N],z1[i-1,M] == z1[i,1])
@NLconstraint(m,g20[i in 2:N],z2[i-1,M] == z2[i,1])
@NLconstraint(m,g21[i in 2:N],z3[i-1,M] == z3[i,1])
@NLconstraint(m,g22[i in 2:N],z4[i-1,M] == z4[i,1])
@NLconstraint(m,g23[i in 2:N],z5[i-1,M] == z5[i,1])
@NLconstraint(m,g24[i in 2:N],z6[i-1,M] == z6[i,1])
@NLconstraint(m,g25[i in 2:N],z7[i-1,M] == z7[i,1])
@NLconstraint(m,g26[i in 2:N],z8[i-1,M] == z8[i,1])
@NLconstraint(m,g27[i in 2:N],λ1[i-1,M] == λ1[i,1])
@NLconstraint(m,g28[i in 2:N],λ2[i-1,M] == λ2[i,1])
@NLconstraint(m,g29[i in 2:N],λ3[i-1,M] == λ3[i,1])
@NLconstraint(m,g30[i in 2:N],λ4[i-1,M] == λ4[i,1])
@NLconstraint(m,g31[i in 2:N],λ5[i-1,M] == λ5[i,1])
@NLconstraint(m,g32[i in 2:N],λ6[i-1,M] == λ6[i,1])
@NLconstraint(m,g33[i in 2:N],λ7[i-1,M] == λ7[i,1])
@NLconstraint(m,g34[i in 2:N],λ8[i-1,M] == λ8[i,1])

# - Linear expression for u1 and u2
@NLconstraint(m,gu1[i in ni,j in 2:(M-1)],u1[i,j] == u1[i,1]
                                                    + (u1[i,M]-u1[i,1])*zc[j])

@NLconstraint(m,gu2[i in ni,j in 2:(M-1)],u2[i,j] == u2[i,1]
                                                    + (u2[i,M]-u2[i,1])*zc[j])

# - Boundary values
@NLconstraint(m,g37,z1[1,1] == z1in)
@NLconstraint(m,g38,z2[1,1] == z2in)
@NLconstraint(m,g39,z3[1,1] == z3in)
@NLconstraint(m,g40,z4[1,1] == z4in)
@NLconstraint(m,g41,z5[1,1] == z5in)
@NLconstraint(m,g42,z6[1,1] == z6in)
@NLconstraint(m,g43,z7[1,1] == z7in)
@NLconstraint(m,g44,z8[1,1] == z8in)
@NLconstraint(m,g45,z8[N,M] == z8f)
@NLconstraint(m,g46,λ1[N,M] == -z4[N,M])
@NLconstraint(m,g47,λ2[N,M] == λ2f)
@NLconstraint(m,g48,λ3[N,M] == λ3f)
@NLconstraint(m,g49,λ4[N,M] == -z1[N,M])
@NLconstraint(m,g50,λ5[N,M] == λ5f)
@NLconstraint(m,g51,λ6[N,M] == λ6f)
@NLconstraint(m,g52,λ7[N,M] == λ7f)

# - Transversality condition on H(tf)
@NLconstraint(m,Hf,Ham[N,M] == z4[N,M]*(u1[N,M] + u2[N,M])
        + z1[N,M]*(f2[N,M]*z2[N,M] - (u1[N,M]+u2[N,M])*z4[N,M]/z1[N,M]))

# - Continuity of H(t)
@NLconstraint(m,Hc[i in 1:(N-1)],Ham[i+1,1] == Ham[i,M])

# Solution
optimize!(m)

z1s = transpose(convert(Array,value.(z1[:,:])))[:]
z2s = transpose(convert(Array,value.(z2[:,:])))[:]
z3s = transpose(convert(Array,value.(z3[:,:])))[:]
z4s = transpose(convert(Array,value.(z4[:,:])))[:]
z5s = transpose(convert(Array,value.(z5[:,:])))[:]
z6s = transpose(convert(Array,value.(z6[:,:])))[:]
z7s = transpose(convert(Array,value.(z7[:,:])))[:]
z8s = transpose(convert(Array,value.(z8[:,:])))[:]
Hs  = transpose(convert(Array,value.(Ham[:,:])))[:]
u1s = transpose(convert(Array,value.(u1[:,:])))[:]
u2s = transpose(convert(Array,value.(u2[:,:])))[:]
μ11s = transpose(convert(Array,value.(μ11[:,:])))[:]   
μ12s = transpose(convert(Array,value.(μ12[:,:])))[:]  
μ21s = transpose(convert(Array,value.(μ21[:,:])))[:]   
μ22s = transpose(convert(Array,value.(μ22[:,:])))[:]  
λ1s = transpose(convert(Array,value.(λ1[:,:])))[:]  
λ2s = transpose(convert(Array,value.(λ2[:,:])))[:] 
λ3s = transpose(convert(Array,value.(λ3[:,:])))[:]
λ4s = transpose(convert(Array,value.(λ4[:,:])))[:]
λ5s = transpose(convert(Array,value.(λ5[:,:])))[:]  
λ6s = transpose(convert(Array,value.(λ6[:,:])))[:] 
λ7s = transpose(convert(Array,value.(λ7[:,:])))[:]
λ8s = transpose(convert(Array,value.(λ8[:,:])))[:]
hs = convert(Array,value.(h[:]))

# Plotting the results
include("plots.jl")


