# This Julia file belongs to the supplementary material of the paper:
#
# "An indirect approach for singular optimal control problems" 
#  - Oswaldo Andrés-Martínez, Lorenz T. Biegler, Antonio Flores-Tlacuahuac
#
# LeeRamirez_initial.jl generates the initial guess for the Lee-Ramirez problem
# by applying a simultaneous direct transcription method
#
#  See readme.1st for more information

using JuMP, Ipopt, DelimitedFiles, PyPlot

let

   # Model definition
   solver = Ipopt.Optimizer
   m = Model(solver)

   include("param.jl")

   # Variables
   @variable(m,z1[i in ni,j in mj],start = z1in)
   @variable(m,z2[i in ni,j in mj],start = z2in)
   @variable(m,z3[i in ni,j in mj],start = z3in)
   @variable(m,z4[i in ni,j in mj],start = z4in)
   @variable(m,z5[i in ni,j in mj],start = z5in)
   @variable(m,z6[i in ni,j in mj],start = z6in)
   @variable(m,z7[i in ni,j in mj],start = z7in)
   @variable(m,uL <= u1[i in ni,j in mj] <= uU)
   @variable(m,uL <= u2[i in ni,j in mj] <= uU)

   # This problem is solved with fixed h
   h = Array{Float64}(undef,N)
   h .= tf/N

   # Auxiliary expressions
   @NLexpression(m,d1[i in ni,j in mj],14.35 + z3[i,j] + z3[i,j]*z3[i,j]/111.5)
   @NLexpression(m,d2[i in ni,j in mj],0.22 + z5[i,j])
   @NLexpression(m,d3[i in ni,j in mj],0.22*z7[i,j]/d2[i,j] + z6[i,j])
   @NLexpression(m,f1[i in ni,j in mj],z3[i,j]*d3[i,j]/d1[i,j])
   @NLexpression(m,f2[i in ni,j in mj],
                            0.233*z3[i,j]*(0.0005+z5[i,j])/(0.022+z5[i,j])/d1[i,j])
   @NLexpression(m,f3[i in ni,j in mj],0.09*z5[i,j]/(0.034+z5[i,j]))

   # Expressions for spline interpolation
   @NLexpression(m,Δu1p[i in ni],u1[i,M]-u1[i,p])
   @NLexpression(m,Δu10[i in ni],u1[i,p]-u1[i,1])
   @NLexpression(m,Δτp,zc[end]-zc[p])
   @NLexpression(m,Δτ0,zc[p]-zc[1])
   @NLexpression(m,C1p[i in ni],3*(Δu1p[i]/Δτp - Δu10[i]/Δτ0)/(2*(Δτ0+Δτp)))
   @NLexpression(m,b10[i in ni],Δu10[i]/Δτ0 - Δτ0*C1p[i]/3)
   @NLexpression(m,b1p[i in ni],Δu1p[i]/Δτp - 2*Δτp*C1p[i]/3)
   @NLexpression(m,d10[i in ni],C1p[i]/(3*Δτ0))
   @NLexpression(m,d1p[i in ni],-C1p[i]/(3*Δτp))

   @NLexpression(m,Δu2p[i in ni],u2[i,M]-u2[i,p])
   @NLexpression(m,Δu20[i in ni],u2[i,p]-u2[i,1])
   @NLexpression(m,C2p[i in ni],3*(Δu2p[i]/Δτp - Δu20[i]/Δτ0)/(2*(Δτ0+Δτp)))
   @NLexpression(m,b20[i in ni],Δu20[i]/Δτ0 - Δτ0*C2p[i]/3)
   @NLexpression(m,b2p[i in ni],Δu2p[i]/Δτp - 2*Δτp*C2p[i]/3)
   @NLexpression(m,d20[i in ni],C2p[i]/(3*Δτ0))
   @NLexpression(m,d2p[i in ni],-C2p[i]/(3*Δτp))

   # Objective function
   @NLobjective(m,Min,-z4[N,M]*z1[N,M])

   # Constraints
   # - States equations
   @NLconstraint(m,g1[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*z1[i,k] for k in mk) 
                                                                 == u1[i,j]+u2[i,j])

   @NLconstraint(m,g2[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*z2[i,k] for k in mk) 
                             == f1[i,j]*z2[i,j] - (u1[i,j]+u2[i,j])*z2[i,j]/z1[i,j])

   @NLconstraint(m,g3[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*z3[i,k] for k in mk)
     == c1*u1[i,j]/z1[i,j] - (u1[i,j]+u2[i,j])*z3[i,j]/z1[i,j] - f1[i,j]*z2[i,j]/c2)

   @NLconstraint(m,g4[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*z4[i,k] for k in mk) 
                             == f2[i,j]*z2[i,j] - (u1[i,j]+u2[i,j])*z4[i,j]/z1[i,j])

   @NLconstraint(m,g5[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*z5[i,k] for k in mk) 
                          == c3*u2[i,j]/z1[i,j] - (u1[i,j]+u2[i,j])*z5[i,j]/z1[i,j])
   @NLconstraint(m,g6[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*z6[i,k] for k in mk) 
                                                                == -f3[i,j]*z6[i,j])
   @NLconstraint(m,g7[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*z7[i,k] for k in mk)
                                                             == f3[i,j]*(1-z7[i,j]))
      

   # - Continuity condition on state variables
   @NLconstraint(m,g9[i in 2:N],z1[i-1,M] == z1[i,1])
   @NLconstraint(m,g10[i in 2:N],z2[i-1,M] == z2[i,1])
   @NLconstraint(m,g11[i in 2:N],z3[i-1,M] == z3[i,1])
   @NLconstraint(m,g12[i in 2:N],z4[i-1,M] == z4[i,1])
   @NLconstraint(m,g13[i in 2:N],z5[i-1,M] == z5[i,1])
   @NLconstraint(m,g14[i in 2:N],z6[i-1,M] == z6[i,1])
   @NLconstraint(m,g15[i in 2:N],z7[i-1,M] == z7[i,1])

   # - Cubic spline interpolation for u1 and u2
   @NLconstraint(m,g17a[i in ni,j in 2:(p-1)],u1[i,j] ==
                      u1[i,1] + b10[i]*(zc[j]-zc[1]) + d10[i]*(zc[j]-zc[1])^3)

   @NLconstraint(m,g17b[i in ni,j in (p+1):(M-1)],u1[i,j] == u1[i,p] 
           + b1p[i]*(zc[j]-zc[p]) + C1p[i]*(zc[j]-zc[p])^2 + d1p[i]*(zc[j]-zc[p])^3)

   @NLconstraint(m,g18a[i in ni,j in 2:(p-1)],u2[i,j] ==
                      u2[i,1] + b20[i]*(zc[j]-zc[1]) + d20[i]*(zc[j]-zc[1])^3)

   @NLconstraint(m,g18b[i in ni,j in (p+1):(M-1)],u2[i,j] == u2[i,p] 
            + b2p[i]*(zc[j]-zc[p]) + C2p[i]*(zc[j]-zc[p])^2 + d2p[i]*(zc[j]-zc[p])^3)

   # - Boundary values 
   @NLconstraint(m,z1iv,z1[1,1] == z1in)
   @NLconstraint(m,z2iv,z2[1,1] == z2in)
   @NLconstraint(m,z3iv,z3[1,1] == z3in)
   @NLconstraint(m,z4iv,z4[1,1] == z4in)
   @NLconstraint(m,z5iv,z5[1,1] == z5in)
   @NLconstraint(m,z6iv,z6[1,1] == z6in)
   @NLconstraint(m,z7iv,z7[1,1] == z7in)

   # Solution
   optimize!(m)

   # Reading results
   z1s = transpose(convert(Array,value.(z1[:,:])))[:]
   z2s = transpose(convert(Array,value.(z2[:,:])))[:]
   z3s = transpose(convert(Array,value.(z3[:,:])))[:]
   z4s = transpose(convert(Array,value.(z4[:,:])))[:]
   z5s = transpose(convert(Array,value.(z5[:,:])))[:]
   z6s = transpose(convert(Array,value.(z6[:,:])))[:]
   z7s = transpose(convert(Array,value.(z7[:,:])))[:]
   u1s = transpose(convert(Array,value.(u1[:,:])))[:]
   u2s = transpose(convert(Array,value.(u2[:,:])))[:]
   hs = h

   # - Duals
   λ1s = Array{Float64}(undef,N,M)
   λ2s = Array{Float64}(undef,N,M)
   λ3s = Array{Float64}(undef,N,M)
   λ4s = Array{Float64}(undef,N,M)
   λ5s = Array{Float64}(undef,N,M)
   λ6s = Array{Float64}(undef,N,M)
   λ7s = Array{Float64}(undef,N,M)

   for i in ni
      for j in mj2
         λ1s[i,j] = dual(g1[i,j])/(hs[i]*w[j])
         λ2s[i,j] = dual(g2[i,j])/(hs[i]*w[j])
         λ3s[i,j] = dual(g3[i,j])/(hs[i]*w[j])
         λ4s[i,j] = dual(g4[i,j])/(hs[i]*w[j])
         λ5s[i,j] = dual(g5[i,j])/(hs[i]*w[j])
         λ6s[i,j] = dual(g6[i,j])/(hs[i]*w[j])
         λ7s[i,j] = dual(g7[i,j])/(hs[i]*w[j])
      end
   end

   λ1s[2:N,1] = λ1s[1:(N-1),M] 
   λ2s[2:N,1] = λ2s[1:(N-1),M] 
   λ3s[2:N,1] = λ3s[1:(N-1),M] 
   λ4s[2:N,1] = λ4s[1:(N-1),M] 
   λ5s[2:N,1] = λ5s[1:(N-1),M] 
   λ6s[2:N,1] = λ6s[1:(N-1),M] 
   λ7s[2:N,1] = λ7s[1:(N-1),M] 


   λ1s = transpose(λ1s)[:]
   λ2s = transpose(λ2s)[:]
   λ3s = transpose(λ3s)[:]
   λ4s = transpose(λ4s)[:]
   λ5s = transpose(λ5s)[:]
   λ6s = transpose(λ6s)[:]
   λ7s = transpose(λ7s)[:]

   λ1s[1] = λ1s[2]
   λ2s[1] = λ2s[2]
   λ3s[1] = λ3s[2]
   λ4s[1] = λ4s[2]
   λ5s[1] = λ5s[2]
   λ6s[1] = λ6s[2]
   λ7s[1] = λ7s[2]

   # Time vector
   t = Array{Float64}(undef,N*M,1)
   for i in 1:N
	   for j in 1:M
         if i == 1
            tpre = 0
         else
            tpre = t[M*(i-1)]
         end
         t[j+M*(i-1)] = zc[j]*hs[i] + tpre
	   end
   end

   # Exporting results 
   open("initialx.txt","w") do xin
      for i in 1:N*M
         writedlm(xin,[z1s[i] z2s[i] z3s[i] z4s[i] z5s[i] z6s[i] z7s[i] t[i]])
      end
   end

   open("initialu.txt","w") do uin
      for i in 1:N*M
         writedlm(uin,[u1s[i] u2s[i]])
      end
   end

   open("initiald.txt","w") do din
      for i in 1:N*M
         writedlm(din,[λ1s[i] λ2s[i] λ3s[i] λ4s[i] λ5s[i] λ6s[i] λ7s[i]])
      end
   end

   # Plotting
   close("all")
   figure()
   plot(t,z1s,t,z2s,t,z3s/10,t,z4s,t,z5s,t,z6s,t,z7s,linewidth=2)
   xlabel(L"t",fontsize=14)
   axis([0,10,-1,9])
   legend([L"z_1",L"z_2",L"z_3/10",L"z_4",L"z_5",L"z_6",L"z_7"],ncol=3,fontsize=12)
   ax = gca()
   setp(ax[:get_yticklabels](),fontsize=12)
   setp(ax[:get_xticklabels](),fontsize=12)
   savefig("states_initial.pdf")

   figure()
   plot(t,u1s,t,u2s,linewidth=2)
   xlabel(L"t",fontsize=14)
   axis([0,10,0,1])
   legend([L"u_1",L"u_2"],fontsize=12)
   ax = gca()
   setp(ax[:get_yticklabels](),fontsize=12)
   setp(ax[:get_xticklabels](),fontsize=12)
   savefig("controls_initial.pdf")
end
 
