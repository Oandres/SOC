# This file generates the coefficients of the Butcher tableau 
# for roots of jacobi polynomials (legendre, radau, lobatto)
#
# Oswaldo Andrés-Martínez
# oswxandres@gmail.com

using Jacobi, SparseArrays

function butcher(M,poly)
   # Zeros of each polynomial in matrix form
   n = M-2;    # Degree
   zr = Array{Float64}(undef,n+1,1);
   zr[end,1] = 1;
      if poly == "Legendre"
         zr[1:end-1,1] = jacobi_zeros(n,0,0);  # Legendre
      elseif poly == "Radau"
         zr[1:end-1,1] = jacobi_zeros(n,1,0); # Radau
      elseif poly == "Lobbato"
         zr[1:end-1,1] = jacobi_zeros(n,1,1); # Lobbato
      else
         error("poly must have one of these: Radau,Legendre, Lobbato")
      end

   # In the interval [0,1]
   cr = (1/2)*zr .+ 1/2;

   # ========= Calculating coefficients b ===========
   BR = Array{Float64}(undef,n+1,n+1)
   for i in 1:n+1
      BR[i,:] = cr'.^(i-1)  # Matrix of coefficents c
   end

   # RHS vector
   bk = Array{Float64}(undef,n+1,1)
   for k in 0:n
      bk[k+1] = 1/(k+1)
   end

   br = BR\bk

   # # ========= Calculating coefficients a ===========
   AR = spzeros((n+1)*(n+1),(n+1)*(n+1))
   j = 1
   for i in 1:(n+1)*(n+1)
      Idc = 1+(j-1)*(n+1):j*(n+1)   # Index set for columns of A
      pc  = Int(floor((i-1)/(n+1))) # Powers for c's
      AR[i,Idc] = cr'.^pc
      if j == (n+1)
         j = 1
      else
         j = j+1
      end
   end

   # RHS vector
   ak = Array{Float64}(undef,(n+1)*(n+1),1)
   for k in 1:(n+1)
      Id = 1+(k-1)*(n+1):k*(n+1)
      ak[Id,1] = (1/k)*cr.^k
   end

   ar = AR\ak
   Ar = Array{Float64}(undef,n+1,n+1)
   for i in 1:n+1
      Ar[i,:] = ar[1+(i-1)*(n+1):i*(n+1)]
   end
   
   return AR,br,cr
end


 

