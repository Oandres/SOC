# Size parameters
N = 30;    # Number of finite elements
M = 7;     # Number of collocation points (including both ends) 

# Sets
ni = 1:N;
mj = 1:M;
mk = copy(mj);
mj2 = 2:M
p = 4 # Inner point for spline interp

# Penalty parameter
ρ    = 100 

# Model parameters
c1 = 100
c2 = 0.51
c3 = 4

# Problem data
z1in = 1   
z2in = 0.1 
z3in = 40  
z4in = 0  
z5in = 0   
z6in = 1
z7in = 0
z8in = 0
z8f  = 10
tf  = 10
uL   = 0  
uU   = 1   

# Transversality conditions for the adjoint variables
λ2f  = 0
λ3f  = 0
λ5f  = 0
λ6f  = 0
λ7f  = 0 

# Collocation matrices
include("matrix.jl")       # It provides the collocation matrx and zeros
A,zc = colmatrix(M,"Radau")

# Weights associated with Radau quadrature
include("tableau.jl")      
Tab,ws,rs = butcher(M,"Radau");
w = Array{Float64}(undef,M)
w[2:M] = ws
