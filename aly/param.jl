  
# Size parameters
N = 30    # Number of finite elements
M = 7     # Number of collocation points (including both ends)

# Sets
ni = 1:N;
mj = 1:M;
mk = copy(mj);
mj2 = 2:M

# Inner points for spline interp
p = 4

# Penalty parameter
ρ    = 100 

# Problem data
x1in = 0   # Initial value for x1
x2in = 1   # Initial value for x2
x3in = 0   # Initial value for x3
x4in = 0   # Initial value for x4
x4f  = 5   # Final value for x4
λ1f  = 0   # Final value for λ1
λ2f  = 0   # Final value for λ2
λ3f  = 1   # Final value for λ3
uL   = -1  # Lower bound for u
uU   = 1   # Upper bound for u

# Collocation matrices
include("matrix.jl")       # It provides the collocation matrx and zeros
A,zc = colmatrix(M,"Radau");
# Weights associated with Radau quadrature
include("tableau.jl")      
Tab,ws,rs = butcher(M,"Radau");
w = Array{Float64}(undef,M)
w[2:M] = ws
