# Initial guess previously obtained 
Zin = readdlm("initialx.txt",'\t')
uin = readdlm("initialu.txt")
hin = readdlm("initialh.txt")
λin = readdlm("initiald.txt",'\t')
z10 = Zin[:,1]
z20 = Zin[:,2]
z30 = Zin[:,3]
z40 = Zin[:,4]
λ10 = λin[:,1]
λ20 = λin[:,2]
λ30 = λin[:,3]
λ40 = λin[:,4]

# Set initial guess
for i in ni
   for j in mj
      set_start_value(z1[i,j],z10[j+M*(i-1)])
   end
end

for i in ni
   for j in mj
      set_start_value(z2[i,j],z20[j+M*(i-1)])
   end
end

for i in ni
   for j in mj
      set_start_value(z3[i,j],z30[j+M*(i-1)])
   end
end

for i in ni
   for j in mj
      set_start_value(z4[i,j],z40[j+M*(i-1)])
   end
end

for i in ni
   for j in mj
      set_start_value(λ1[i,j],λ10[j+M*(i-1)])
      set_start_value(λ1[1,1],λ10[2])
   end
end

for i in ni
   for j in mj
      set_start_value(λ2[i,j],λ20[j+M*(i-1)])
      set_start_value(λ2[1,1],λ20[2])
   end
end

for i in ni
   for j in mj
      set_start_value(λ3[i,j],λ30[j+M*(i-1)])
      set_start_value(λ3[1,1],λ30[2])
   end
end

for i in ni
   for j in mj
      set_start_value(λ4[i,j],λ40[j+M*(i-1)])
      set_start_value(λ4[1,1],λ40[2])
   end
end


for i in ni
   for j in mj
      set_start_value(u[i,j],uin[j+M*(i-1)])
   end
end

