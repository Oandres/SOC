# This Julia file belongs to the supplementary material of the paper:
#
# "An indirect approach for singular optimal control problems" 
#  - Oswaldo Andrés-Martínez, Lorenz T. Biegler, Antonio Flores-Tlacuahuac
#
# aly_initial.jl generates the initial guess for the Aly problem
# by applying a simultaneous direct transcription method
#
#  See readme.1st for more information


using JuMP, Ipopt, DelimitedFiles, PyPlot

let

   # Model definition
   solver = Ipopt.Optimizer
   m = Model(solver)

   include("param.jl")

   # Variables
   @variable(m,x1[i in ni,j in mj],start = x1in)
   @variable(m,x2[i in ni,j in mj],start = x2in)
   @variable(m,x3[i in ni,j in mj],start = x3in)
   @variable(m,x4[i in ni,j in mj],start = x4in)
   @variable(m,uL <= u[i in ni,j in mj] <= uU)
   @variable(m,0.0002 <= h[i in ni]<= 5.4/N,start=5/N)


   # Expressions for spline interpolation
   @NLexpression(m,Δup[i in ni],u[i,M]-u[i,p])
   @NLexpression(m,Δu0[i in ni],u[i,p]-u[i,1])
   @NLexpression(m,Δτp,zc[end]-zc[p])
   @NLexpression(m,Δτ0,zc[p]-zc[1])
   @NLexpression(m,Cp[i in ni],3*(Δup[i]/Δτp - Δu0[i]/Δτ0)/(2*(Δτ0+Δτp)))
   @NLexpression(m,b0[i in ni],Δu0[i]/Δτ0 - Δτ0*Cp[i]/3)
   @NLexpression(m,bp[i in ni],Δup[i]/Δτp - 2*Δτp*Cp[i]/3)
   @NLexpression(m,d0[i in ni],Cp[i]/(3*Δτ0))
   @NLexpression(m,dp[i in ni],-Cp[i]/(3*Δτp))

   # Objective function
   @NLobjective(m,Min,x3[N,M])

   # Constraints
   # - States equations
   @NLconstraint(m,g1[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*x1[i,k] for k in mk) 
                                                                      == x2[i,j])
   @NLconstraint(m,g2[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*x2[i,k] for k in mk) 
                                                                      == u[i,j])
   @NLconstraint(m,g3[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*x3[i,k] for k in mk)
                                                == (1/2)*(x1[i,j]^2+x2[i,j]^2))
   @NLconstraint(m,g4[i in ni,j in mj2],
                                    (1/h[i])*sum(A[j,k]*x4[i,k] for k in mk) == 1)
          

   # - Continuity condition on state variables
   @NLconstraint(m,g5[i in 2:N],x1[i-1,M] == x1[i,1])
   @NLconstraint(m,g6[i in 2:N],x2[i-1,M] == x2[i,1])
   @NLconstraint(m,g7[i in 2:N],x3[i-1,M] == x3[i,1])
   @NLconstraint(m,g8[i in 2:N],x4[i-1,M] == x4[i,1])

   # - Cubic spline interpolation for u
   @NLconstraint(m,g9a[i in ni,j in 2:(p-1)],u[i,j] ==
                      u[i,1] + b0[i]*(zc[j]-zc[1]) + d0[i]*(zc[j]-zc[1])^3)

   @NLconstraint(m,g9b[i in ni,j in (p+1):(M-1)],u[i,j] == u[i,p] 
             + bp[i]*(zc[j]-zc[p]) + Cp[i]*(zc[j]-zc[p])^2 + dp[i]*(zc[j]-zc[p])^3)

   # - Boundary values 
   @NLconstraint(m,x1iv,x1[1,1] == x1in)
   @NLconstraint(m,x2iv,x2[1,1] == x2in)
   @NLconstraint(m,x3iv,x3[1,1] == x3in)
   @NLconstraint(m,x4iv,x4[1,1] == x4in)
   @NLconstraint(m,x4fv,x4[N,M] == x4f)

   # Solution
   optimize!(m)

   # Reading results
   x1s = transpose(convert(Array,value.(x1[:,:])))[:]
   x2s = transpose(convert(Array,value.(x2[:,:])))[:]
   x3s = transpose(convert(Array,value.(x3[:,:])))[:]
   x4s = transpose(convert(Array,value.(x4[:,:])))[:]
   us  = transpose(convert(Array,value.(u[:,:])))[:]
   hs = convert(Array,value.(h[:]))

   # - Duals
   λ1s = Array{Float64}(undef,N,M)
   λ2s = Array{Float64}(undef,N,M)
   λ3s = Array{Float64}(undef,N,M)
   λ4s = Array{Float64}(undef,N,M)
   μ1s = Array{Float64}(undef,N,M)
   μ2s = Array{Float64}(undef,N,M)
   for i in ni
      for j in mj2
         λ1s[i,j] = dual(g1[i,j])/(hs[i]*w[j])
         λ2s[i,j] = dual(g2[i,j])/(hs[i]*w[j])
         λ3s[i,j] = dual(g3[i,j])/(hs[i]*w[j])
         λ4s[i,j] = dual(g4[i,j])/(hs[i]*w[j])
      end
   end

   λ1s[2:N,1] = λ1s[1:(N-1),M] 
   λ2s[2:N,1] = λ2s[1:(N-1),M] 
   λ3s[2:N,1] = λ3s[1:(N-1),M] 
   λ4s[2:N,1] = λ4s[1:(N-1),M] 

   λ1s = transpose(λ1s)[:]
   λ2s = transpose(λ2s)[:]
   λ3s = transpose(λ3s)[:]
   λ4s = transpose(λ4s)[:]


   # Exporting results 
   open("initialx.txt","w") do xin
      for i in 1:N*M
         writedlm(xin,[x1s[i] x2s[i] x3s[i] x4s[i]])
      end
   end

   open("initialh.txt","w") do hin
      for i in 1:N
         writedlm(hin,hs[i])
      end
   end

   open("initialu.txt","w") do uin
      for i in 1:N*M
         writedlm(uin,us[i])
      end
   end

   open("initiald.txt","w") do λin
      for i in 1:N*M
         writedlm(λin,[λ1s[i] λ2s[i] λ3s[i] λ4s[i]])
      end
   end

   # Time vector
   t = Array{Float64}(undef,N*M,1)
   for i in 1:N
	   for j in 1:M
         if i == 1
            tpre = 0
         else
            tpre = t[M*(i-1)]
         end
         t[j+M*(i-1)] = zc[j]*hs[i] + tpre
	   end
   end


   # Plotting
   close("all")
   figure()
   plot(t,x1s,t,x2s,linewidth=2)
   xlabel(L"t",fontsize=14)
   legend([L"z_1",L"z_2"],fontsize=12)
   ax = gca()
   setp(ax[:get_yticklabels](),fontsize=12)
   setp(ax[:get_xticklabels](),fontsize=12)
   savefig("states_initial.pdf")

   figure()
   plot(t,us,linewidth=2)
   xlabel(L"t",fontsize=14)
   legend([L"u"],fontsize=12)
   axis([0,t[end],uL,uU])
   ax = gca()
   setp(ax[:get_yticklabels](),fontsize=12)
   setp(ax[:get_xticklabels](),fontsize=12)
   savefig("control_initial.pdf")
end





 
