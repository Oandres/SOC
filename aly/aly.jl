# This Julia file belongs to the supplementary material of the paper:
#
# "An indirect approach for singular optimal control problems" 
#  - Oswaldo Andrés-Martínez, Lorenz T. Biegler, Antonio Flores-Tlacuahuac
#
# aly.jl solves the Aly problem by applying an indirect approach
#
# See readme.md for more information

using JuMP, Ipopt, PyPlot, DelimitedFiles, LinearAlgebra

# Model definition
solver = Ipopt.Optimizer
m = Model(solver)

include("param.jl")

# Variables
@variable(m,z1[i in ni,j in mj])
@variable(m,z2[i in ni,j in mj])
@variable(m,z3[i in ni,j in mj])
@variable(m,z4[i in ni,j in mj])
@variable(m,λ1[i in ni,j in mj])
@variable(m,λ2[i in ni,j in mj])
@variable(m,λ3[i in ni,j in mj])
@variable(m,λ4[i in ni,j in mj])
@variable(m,Ham[i in ni,j in mj])
@variable(m,μ1[i in ni,j in mj]>=0)
@variable(m,μ2[i in ni,j in mj]>=0)
@variable(m,uL <= u[i in ni,j in mj] <= uU)
@variable(m,0.0002 <= h[i in ni]<= 5.4/N,start=5/N)

include("setinitial.jl")

# Expressions for spline interpolation
@NLexpression(m,Δup[i in ni],u[i,M]-u[i,p])
@NLexpression(m,Δu0[i in ni],u[i,p]-u[i,1])
@NLexpression(m,Δτp,zc[end]-zc[p])
@NLexpression(m,Δτ0,zc[p]-zc[1])
@NLexpression(m,Cp[i in ni],3*(Δup[i]/Δτp - Δu0[i]/Δτ0)/(2*(Δτ0+Δτp)))
@NLexpression(m,b0[i in ni],Δu0[i]/Δτ0 - Δτ0*Cp[i]/3)
@NLexpression(m,bp[i in ni],Δup[i]/Δτp - 2*Δτp*Cp[i]/3)
@NLexpression(m,d0[i in ni],Cp[i]/(3*Δτ0))
@NLexpression(m,dp[i in ni],-Cp[i]/(3*Δτp))

# Objective function 
@NLobjective(m,Min,ρ*sum(μ1[i,j]*(1-u[i,j]) for i in ni,j in mj) 
                           + ρ*sum(μ2[i,j]*(1+u[i,j]) for i in ni,j in mj))

# Constraints 
# - State equations
@NLconstraint(m,g1[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*z1[i,k] for k in mk) 
                                                                   == z2[i,j])
@NLconstraint(m,g2[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*z2[i,k] for k in mk) 
                                                                   == u[i,j])
@NLconstraint(m,g3[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*z3[i,k] for k in mk)
                                             == (1/2)*(z1[i,j]^2+z2[i,j]^2))
@NLconstraint(m,g4[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*z4[i,k] for k in mk) == 1)
       
# - Adjoint equations
@NLconstraint(m,g5[i in ni,j in mj2],
              (1/h[i])*sum(A[j,k]*λ1[i,k] for k in mk) == -λ3[i,j]*z1[i,j])
@NLconstraint(m,g6[i in ni,j in mj2],
     (1/h[i])*sum(A[j,k]*λ2[i,k] for k in mk) == -λ3[i,j]*z2[i,j] -λ1[i,j])
@NLconstraint(m,g7[i in ni,j in mj2],
              (1/h[i])*sum(A[j,k]*λ3[i,k] for k in mk) == 0)

@NLconstraint(m,g8[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*λ4[i,k] for k in mk) == 0)


# - Expression for the Hamiltonian
@NLconstraint(m,ham[i in ni,j in mj],Ham[i,j]==λ3[i,j]*(1/2)*(z1[i,j]^2+z2[i,j]^2) 
                         + λ1[i,j]*z2[i,j] + λ2[i,j]*u[i,j] + λ4[i,j]
                                   + μ1[i,j]*(u[i,j]-1) - μ2[i,j]*(1+u[i,j]))

# - dH/du = 0
@NLconstraint(m,g9[i in ni,j in mj],λ2[i,j] + μ1[i,j] - μ2[i,j] == 0)

# - Continuity conditions
@NLconstraint(m,g10[i in 2:N],z1[i-1,M] == z1[i,1])
@NLconstraint(m,g11[i in 2:N],z2[i-1,M] == z2[i,1])
@NLconstraint(m,g12[i in 2:N],z3[i-1,M] == z3[i,1])
@NLconstraint(m,g13[i in 2:N],z4[i-1,M] == z4[i,1])
@NLconstraint(m,g14[i in 2:N],λ1[i-1,M] == λ1[i,1])
@NLconstraint(m,g15[i in 2:N],λ2[i-1,M] == λ2[i,1])
@NLconstraint(m,g16[i in 2:N],λ3[i-1,M] == λ3[i,1])
@NLconstraint(m,g17[i in 2:N],λ4[i-1,M] == λ4[i,1])

# - Cubic spline interpolation for u
@NLconstraint(m,g18a[i in ni,j in 2:(p-1)],u[i,j] ==
                   u[i,1] + b0[i]*(zc[j]-zc[1]) + d0[i]*(zc[j]-zc[1])^3)

@NLconstraint(m,g18b[i in ni,j in (p+1):(M-1)],u[i,j] == u[i,p] 
          + bp[i]*(zc[j]-zc[p]) + Cp[i]*(zc[j]-zc[p])^2 + dp[i]*(zc[j]-zc[p])^3)

# - Boundary values
@NLconstraint(m,g19,z1[1,1] == x1in)
@NLconstraint(m,g20,z2[1,1] == x2in)
@NLconstraint(m,g21,z3[1,1] == x3in)
@NLconstraint(m,g22,z4[1,1] == x4in)
@NLconstraint(m,g23,z4[N,M] == x4f)
@NLconstraint(m,g24,λ1[N,M] == λ1f)
@NLconstraint(m,g25,λ2[N,M] == λ2f)
@NLconstraint(m,g26,λ3[N,M] == λ3f)


# - Transversality condition on H(tf)
@NLconstraint(m,HF,Ham[N,M] == -(1/2)*(z1[N,M]^2+z2[N,M]^2))

# - Continuity of H(t)
@NLconstraint(m,CH[i in 2:N],Ham[i-1,M] == Ham[i,1]) 

# Solution
optimize!(m)

z1s = transpose(convert(Array,value.(z1[:,:])))[:]
z2s = transpose(convert(Array,value.(z2[:,:])))[:]
z3s = transpose(convert(Array,value.(z3[:,:])))[:]
z4s = transpose(convert(Array,value.(z4[:,:])))[:]
Hs  = transpose(convert(Array,value.(Ham[:,:])))[:]
us  = transpose(convert(Array,value.(u[:,:])))[:]   
μ1s = transpose(convert(Array,value.(μ1[:,:])))[:]   
μ2s = transpose(convert(Array,value.(μ2[:,:])))[:]  
λ1s = transpose(convert(Array,value.(λ1[:,:])))[:]  
λ2s = transpose(convert(Array,value.(λ2[:,:])))[:] 
λ3s = transpose(convert(Array,value.(λ3[:,:])))[:]
λ4s = transpose(convert(Array,value.(λ4[:,:])))[:]
hs = convert(Array,value.(h[:]))

# Plotting the results
include("plots.jl")


