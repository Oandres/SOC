# Singular Optimal Control

This folder contains the Julia files that belong to the Supplementary Material of
the paper:

   "An indirect approach for singular optimal control problems" 
    - Oswaldo Andrés-Martínez, Lorenz T. Biegler, Antonio Flores-Tlacuahuac

Each sub-folder contains the solution of one singular optimal control problem. 
Initial guess values must be first obtained by executing the file "name"_initial.jl,
where "name" is the name of the problem. This file will export the results to .txt
files and generate some plots. Then the main problem is solved by executing the file
"name".jl. All files were tested in Julia 1.3.1.

The packages required to run the files are:
JuMP, Ipopt, PyPlot, DelimitedFiles, LinearAlgebra, Jacobi, SparseArrays.

Each sub-folder also includes auxiliary files that are called automatically during
the computation:

matrix.jl     provides a function to generate the collocation matrix A.
tableau.jl    provides a function to generate quadrature weights.
param.jl      provides parameters values for the model and the NLP.
setinitial.jl sets initial values for the optimization variables.
plots.jl      calculates the switching function, the optimality condition of H(t)
              with the results and generates the corresponding plots. 
ipopt.opt     sets the linear solver and the maximum number of iterations.
              If MA27 or similar is not installed, the default linear solver MUMPS
              can be used for simple problems by changing the value or removing
              this file. However, there is no guaratee that the solution will match
              the one reported in the paper, and some large problems might not
              even converge.
