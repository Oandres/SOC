# This file generates the plots with the results of the main program.
# Variables zc are read from the execution of file matrix.jl. 

using PyPlot

close("all")
# Plots

# Time vector

t = Array{Float64}(undef,N*M,1)
for i in 1:N
	for j in 1:M
      if i == 1
         tpre = 0
      else
         tpre = t[M*(i-1)]
      end
      t[j+M*(i-1)] = zc[j]*hs[i] + tpre
	end
end

figure()
plot(t,x1s/100,t,x2s/100,t,x3s,t,x4s/10,linewidth=2)
xlabel(L"t",fontsize=14)
legend([L"z_1/100",L"z_2/100",L"z_3",L"z_4/10"],fontsize=12)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)
savefig("states.pdf")

figure()
plot(t,λ1s/100,t,λ2s/100,t,λ3s,linewidth=2)
xlabel(L"t",fontsize=14)
legend([L"\lambda_1",L"\lambda_2",L"\lambda_3"],fontsize=12)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)
savefig("adjoints.pdf")

figure()
plot(t,μ11s,t,μ12s,t,μ21s,t,μ22s,linewidth=2)
xlabel(L"t",fontsize=14)
legend([L"\mu_{11}",L"\mu_{12}",L"\mu_{21}",L"\mu_{22}"],fontsize=12)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)
savefig("multipliers.pdf")

figure()
plot(t,u1s,t,u2s,linewidth=2)
xlabel(L"t",fontsize=14)
legend([L"u_1",L"u_2"],fontsize=12)
#axis([0,t[end],uL,uU])
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)
savefig("controls.pdf")

# Hu1 & Hu2
Hu1 = -λ2s*γ.*x2s .+ λ3s .+ μ11s .- μ12s
Hu2 = -λ1s*ϕ.*x1s .- λ2s*η.*x2s .+ λ4s .+ μ21s .- μ22s

# Switching functions 
ϕ1 = -λ2s*γ.*x2s .+ λ3s 
ϕ2 = -λ1s*ϕ.*x1s .- λ2s*η.*x2s .+ λ4s

figure()
plot(t,Hs/10000,t,Hu1,t,Hu2,linewidth=2)
xlabel(L"t",fontsize=14)
legend([L"H/10000",L"H_{u_1}",L"H_{u_2}"],fontsize=12)
#axis([0,t[end],-1e-3,1e-3])
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)
savefig("Hu.pdf")

figure()
plot(t,ϕ1,t,ϕ2,linewidth=2)
xlabel(L"t",fontsize=14)
legend([L"\sigma_1",L"\sigma_2"],fontsize=12)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)
savefig("switchingf.pdf")
