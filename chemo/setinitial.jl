# Initial guess previously obtained 
Xin = readdlm("initialx.txt",'\t')
uin = readdlm("initialu.txt",'\t')
λin = readdlm("initiald.txt",'\t')
ηin = readdlm("initialn.txt",'\t')
hin = readdlm("initialh.txt")
x10 = Xin[:,1]
x20 = Xin[:,2]
x30 = Xin[:,3]
x40 = Xin[:,4]
x50 = Xin[:,5]
λ10 = λin[:,1]
λ20 = λin[:,2]
λ30 = λin[:,3]
λ40 = λin[:,4]
λ50 = λin[:,5]
u10 = uin[:,1]
u20 = uin[:,2]
η1s = ηin[1,1]
η2s = ηin[1,2]

# Set initial guess
for i in ni
   for j in mj
      set_start_value(x1[i,j],x10[j+M*(i-1)])
   end
end

for i in ni
   for j in mj
      set_start_value(x2[i,j],x20[j+M*(i-1)])
   end
end

for i in ni
   for j in mj
      set_start_value(x3[i,j],x30[j+M*(i-1)])
   end
end

for i in ni
   for j in mj
      set_start_value(x4[i,j],x40[j+M*(i-1)])
   end
end

for i in ni
   for j in mj
      set_start_value(x5[i,j],x50[j+M*(i-1)])
   end
end

for i in ni
   for j in mj
      set_start_value(λ1[i,j],λ10[j+M*(i-1)])
      set_start_value(λ1[1,1],λ10[2])
   end
end

for i in ni
   for j in mj
      set_start_value(λ2[i,j],λ20[j+M*(i-1)])
      set_start_value(λ2[1,1],λ20[2])
   end
end

for i in ni
   for j in mj
      set_start_value(λ3[i,j],λ30[j+M*(i-1)])
      set_start_value(λ3[1,1],λ30[2])
   end
end

for i in ni
   for j in mj
      set_start_value(λ4[i,j],λ40[j+M*(i-1)])
      set_start_value(λ4[1,1],λ40[2])
   end
end

for i in ni
   for j in mj
      set_start_value(λ5[i,j],λ50[j+M*(i-1)])
      set_start_value(λ5[1,1],λ50[2])
   end
end

for i in ni
   for j in mj
      set_start_value(u1[i,j],u10[j+M*(i-1)])
   end
end

for i in ni
   for j in mj
      set_start_value(u2[i,j],u20[j+M*(i-1)])
   end
end

set_start_value(η1,η1s)
set_start_value(η2,η2s)

#for i in ni
#   set_start_value(h[i],hin[i])
#end


