# This Julia file belongs to the supplementary material of the paper:
#
# "An indirect approach for singular optimal control problems" 
#  - Oswaldo Andrés-Martínez, Lorenz T. Biegler, Antonio Flores-Tlacuahuac
#
# chemo_initial.jl generates the initial guess for the Chemotherapy problem
# by applying a simultaneous direct transcription method
#
#  See readme.1st for more information

using JuMP, Ipopt, DelimitedFiles, PyPlot

let

   # Model definition
   solver = Ipopt.Optimizer
   m = Model(solver)

   include("param.jl")

   # Variables
   @variable(m,x1[i in ni,j in mj],start = x1in)
   @variable(m,x2[i in ni,j in mj],start = x2in)
   @variable(m,x3[i in ni,j in mj],start = x3in)
   @variable(m,x4[i in ni,j in mj],start = x4in)
   @variable(m,x5[i in ni,j in mj],start = x5in)
   @variable(m,u1L <= u1[i in ni,j in mj] <= u1U)
   @variable(m,u2L <= u2[i in ni,j in mj] <= u2U)
   @variable(m,0.0001 <= h[i in ni] <= 0.3, start=x5f/N)

   # Expressions for spline interpolation
   @NLexpression(m,Δu1p[i in ni],u1[i,M]-u1[i,p])
   @NLexpression(m,Δu10[i in ni],u1[i,p]-u1[i,1])
   @NLexpression(m,Δτp,zc[end]-zc[p])
   @NLexpression(m,Δτ0,zc[p]-zc[1])
   @NLexpression(m,C1p[i in ni],3*(Δu1p[i]/Δτp - Δu10[i]/Δτ0)/(2*(Δτ0+Δτp)))
   @NLexpression(m,b10[i in ni],Δu10[i]/Δτ0 - Δτ0*C1p[i]/3)
   @NLexpression(m,b1p[i in ni],Δu1p[i]/Δτp - 2*Δτp*C1p[i]/3)
   @NLexpression(m,d10[i in ni],C1p[i]/(3*Δτ0))
   @NLexpression(m,d1p[i in ni],-C1p[i]/(3*Δτp))

   @NLexpression(m,Δu2p[i in ni],u2[i,M]-u2[i,p])
   @NLexpression(m,Δu20[i in ni],u2[i,p]-u2[i,1])
   @NLexpression(m,C2p[i in ni],3*(Δu2p[i]/Δτp - Δu20[i]/Δτ0)/(2*(Δτ0+Δτp)))
   @NLexpression(m,b20[i in ni],Δu20[i]/Δτ0 - Δτ0*C2p[i]/3)
   @NLexpression(m,b2p[i in ni],Δu2p[i]/Δτp - 2*Δτp*C2p[i]/3)
   @NLexpression(m,d20[i in ni],C2p[i]/(3*Δτ0))
   @NLexpression(m,d2p[i in ni],-C2p[i]/(3*Δτp))

   # Objective function
   @NLobjective(m,Min,x1[N,M])

   # Constraints
   # - States equations
   @NLconstraint(m,g1[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*x1[i,k] for k in mk) ==
                           -ζ*x1[i,j]*log(x1[i,j]/x2[i,j]) - ϕ*x1[i,j]*u2[i,j])


   @NLconstraint(m,g2[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*x2[i,k] for k in mk) ==
             b*x2[i,j]^(2/3) - d*x2[i,j]^(4/3) - μ*x2[i,j] - γ*x2[i,j]*u1[i,j]
                         - η*x2[i,j]*u2[i,j])

   @NLconstraint(m,g3[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*x3[i,k] for k in mk) 
                                                                  == u1[i,j])

   @NLconstraint(m,g4[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*x4[i,k] for k in mk)
                                                                  == u2[i,j])

   @NLconstraint(m,g5[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*x5[i,k] for k in mk) 
                                                                           == 1)

   # - Continuity condition on state variables
   @NLconstraint(m,g6[i in 2:N],x1[i-1,M] == x1[i,1])
   @NLconstraint(m,g7[i in 2:N],x2[i-1,M] == x2[i,1])
   @NLconstraint(m,g8[i in 2:N],x3[i-1,M] == x3[i,1])
   @NLconstraint(m,g9[i in 2:N],x4[i-1,M] == x4[i,1])
   @NLconstraint(m,g10[i in 2:N],x5[i-1,M] == x5[i,1])

   # - Cubic spline interpolation for u1 and u2
   @NLconstraint(m,gu1[i in ni,j in 2:(M-1)],u1[i,j] == u1[i,1]
                                                    + (u1[i,M]-u1[i,1])*zc[j])

   @NLconstraint(m,gu2[i in ni,j in 2:(M-1)],u2[i,j] == u2[i,1]
                                                    + (u2[i,M]-u2[i,1])*zc[j])


   # - Constraints of final time for some x
   @NLconstraint(m,g13,x3[N,M] <= 45)
   @NLconstraint(m,g14,x4[N,M] <= 135)

   # - Boundary values
   @NLconstraint(m,g15,x1[1,1] == x1in)
   @NLconstraint(m,g16,x2[1,1] == x2in)
   @NLconstraint(m,g17,x3[1,1] == x3in)
   @NLconstraint(m,g18,x4[1,1] == x4in)
   @NLconstraint(m,g19,x5[1,1] == x5in)
   @NLconstraint(m,g20,x5[N,M] == x5f)


   # Solution
   optimize!(m)

   # Reading results
   x1s = transpose(convert(Array,value.(x1[:,:])))[:]
   x2s = transpose(convert(Array,value.(x2[:,:])))[:]
   x3s = transpose(convert(Array,value.(x3[:,:])))[:]
   x4s = transpose(convert(Array,value.(x4[:,:])))[:]
   x5s = transpose(convert(Array,value.(x5[:,:])))[:]
   u1s = transpose(convert(Array,value.(u1[:,:])))[:]
   u2s = transpose(convert(Array,value.(u2[:,:])))[:]
   hs = convert(Array,value.(h[:]))

   # - Duals
   λ1s = Array{Float64}(undef,N,M)
   λ2s = Array{Float64}(undef,N,M)
   λ3s = Array{Float64}(undef,N,M)
   λ4s = Array{Float64}(undef,N,M)
   λ5s = Array{Float64}(undef,N,M)
   for i in ni
      for j in mj2
         λ1s[i,j] = dual(g1[i,j])/(hs[i]*w[j])
         λ2s[i,j] = dual(g2[i,j])/(hs[i]*w[j])
         λ3s[i,j] = dual(g3[i,j])/(hs[i]*w[j])
         λ4s[i,j] = dual(g4[i,j])/(hs[i]*w[j])
         λ5s[i,j] = dual(g5[i,j])/(hs[i]*w[j])
      end
   end


   λ1s[2:N,1] = λ1s[1:(N-1),M] 
   λ2s[2:N,1] = λ2s[1:(N-1),M] 
   λ3s[2:N,1] = λ3s[1:(N-1),M] 
   λ4s[2:N,1] = λ4s[1:(N-1),M] 
   λ5s[2:N,1] = λ5s[1:(N-1),M] 

   λ1s = transpose(λ1s)[:]
   λ2s = transpose(λ2s)[:]
   λ3s = transpose(λ3s)[:]
   λ4s = transpose(λ4s)[:]
   λ5s = transpose(λ5s)[:]

   η1 = dual(g13)
   η2 = dual(g14)

   if η1 < 0   
      η1s = -η1
   else
      η1s = η1
   end

   if η2 < 0
      η2s = -η2
   else
      η2s = η2
   end

   # Exporting results 
   open("initialx.txt","w") do xin
      for i in 1:N*M
         writedlm(xin,[x1s[i] x2s[i] x3s[i] x4s[i] x5s[i]])
      end
   end

   open("initialu.txt","w") do uin
      for i in 1:N*M
         writedlm(uin,[u1s[i] u2s[i]])
      end
   end

   open("initiald.txt","w") do λin
      for i in 1:N*M
         writedlm(λin,[λ1s[i] λ2s[i] λ3s[i] λ4s[i] λ5s[i]])
      end
   end

   open("initialh.txt","w") do hin 
      for i in 1:N
         writedlm(hin,hs[i])
      end
   end

   open("initialn.txt","w") do ηin
      writedlm(ηin,[η1s η2s])
   end

   # Time vector
   t = Array{Float64}(undef,N*M,1)
   for i in 1:N
	   for j in 1:M
         if i == 1
            tpre = 0
         else
            tpre = t[M*(i-1)]
         end
         t[j+M*(i-1)] = zc[j]*hs[i] + tpre
	   end
   end

   # Plotting
   close("all")
   figure()
   plot(t,x1s/100,t,x2s/100,t,x3s,linewidth=2)
   xlabel(L"t",fontsize=14)
   legend([L"x_1",L"x_2",L"x_3"],fontsize=12)
   ax = gca()
   setp(ax[:get_yticklabels](),fontsize=12)
   setp(ax[:get_xticklabels](),fontsize=12)
   savefig("states_initial.pdf")


   figure()
   plot(t,u1s,t,u2s,linewidth=2)
   xlabel(L"t",fontsize=14)
   legend([L"u_1",L"u_2"],fontsize=12)
   ax = gca()
   setp(ax[:get_yticklabels](),fontsize=12)
   setp(ax[:get_xticklabels](),fontsize=12)
   savefig("controls_initial.pdf")
end


