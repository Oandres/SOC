# This Julia file belongs to the supplementary material of the paper:
#
# "An indirect approach for singular optimal control problems" 
#  - Oswaldo Andrés-Martínez, Lorenz T. Biegler, Antonio Flores-Tlacuahuac
#
# chemo.jl solves the Chemotherapy problem by applying an indirect approach
#
# See readme.md for more information

using JuMP, Ipopt, DelimitedFiles, PyPlot, LinearAlgebra

# Model definition
solver = Ipopt.Optimizer
m = Model(solver)

include("param.jl")

# Variables
@variable(m,x1[i in ni,j in mj])
@variable(m,x2[i in ni,j in mj])
@variable(m,x3[i in ni,j in mj])
@variable(m,x4[i in ni,j in mj])
@variable(m,x5[i in ni,j in mj])
@variable(m,λ1[i in ni,j in mj])
@variable(m,λ2[i in ni,j in mj])
@variable(m,λ3[i in ni,j in mj])
@variable(m,λ4[i in ni,j in mj])
@variable(m,λ5[i in ni,j in mj])
@variable(m,Ham[i in ni,j in mj])
@variable(m,η1>=0)
@variable(m,η2>=0)
@variable(m,μ11[i in ni,j in mj]>=0)
@variable(m,μ12[i in ni,j in mj]>=0)
@variable(m,μ21[i in ni,j in mj]>=0)
@variable(m,μ22[i in ni,j in mj]>=0)
@variable(m,u1L <= u1[i in ni,j in mj] <= u1U)
@variable(m,u2L <= u2[i in ni,j in mj] <= u2U)
@variable(m,0.0001 <= h[i in ni]<= 8.9/N,start=x5f/N)

include("setinitial.jl")

# Objective function
@NLobjective(m,Min,20ρ*sum(μ11[i,j]*(15-u1[i,j]) for i in ni,j in mj)
                    + ρ*sum(μ21[i,j]*(20-u2[i,j]) for i in ni,j in mj)  
                    + 20ρ*sum(μ12[i,j]*(u1[i,j]) for i in ni,j in mj)
                    + ρ*sum(μ22[i,j]*(u2[i,j]) for i in ni,j in mj)
                    + ρ*η1*(45-x3[N,M]) + ρ*η2*(135-x4[N,M]))

# Constraints
# - States equations
@NLconstraint(m,g1[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*x1[i,k] for k in mk) ==
                        -ζ*x1[i,j]*log(x1[i,j]/x2[i,j]) - ϕ*x1[i,j]*u2[i,j])


@NLconstraint(m,g2[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*x2[i,k] for k in mk) ==
          b*x2[i,j]^(2/3) - d*x2[i,j]^(4/3) - μ*x2[i,j] - γ*x2[i,j]*u1[i,j]
                      - η*x2[i,j]*u2[i,j])

@NLconstraint(m,g3[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*x3[i,k] for k in mk) 
                                                               == u1[i,j])

@NLconstraint(m,g4[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*x4[i,k] for k in mk)
                                                               == u2[i,j])

@NLconstraint(m,g5[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*x5[i,k] for k in mk) 
                                                                        == 1)

# - Adjoint equations
@NLconstraint(m,g6[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*λ1[i,k] for k in mk)
                  == -λ1[i,j]*(-ζ*(1+log(x1[i,j]/x2[i,j])) - ϕ*u2[i,j]))

@NLconstraint(m,g7[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*λ2[i,k] for k in mk)
                  == -λ1[i,j]*ζ*x1[i,j]/x2[i,j] - λ2[i,j]*((2/3)*b*x2[i,j]^(-1/3) 
                  - (4/3)*d*x2[i,j]^(1/3) - μ - γ*u1[i,j] - η*u2[i,j]))

@NLconstraint(m,g8[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*λ3[i,k] for k in mk) == 0)

@NLconstraint(m,g9[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*λ4[i,k] for k in mk) == 0)

@NLconstraint(m,g10[i in ni,j in mj2],(1/h[i])*sum(A[j,k]*λ5[i,k] for k in mk) == 0)

# - Expression for the Hamiltonian
@NLconstraint(m,ham[i in ni,j in mj],Ham[i,j] ==
                     λ1[i,j]*(-ζ*x1[i,j]*log(x1[i,j]/x2[i,j]) - ϕ*x1[i,j]*u2[i,j]) 
     + λ2[i,j]*(b*x2[i,j]^(2/3) - d*x2[i,j]^(4/3) - μ*x2[i,j] - γ*x2[i,j]*u1[i,j]
                      - η*x2[i,j]*u2[i,j]) 
     + λ3[i,j]*u1[i,j] + λ4[i,j]*u2[i,j] + λ5[i,j]
     + μ11[i,j]*(u1[i,j]-u1U) - μ12[i,j]*u1[i,j] 
     + μ21[i,j]*(u2[i,j]-u2U) - μ22[i,j]*u2[i,j])

# - dH/du = 0
@NLconstraint(m,g11[i in ni,j in mj],-λ2[i,j]*γ*x2[i,j] + λ3[i,j]  
                                                        + μ11[i,j] - μ12[i,j] == 0)

@NLconstraint(m,g12[i in ni,j in mj],-λ1[i,j]*ϕ*x1[i,j] - λ2[i,j]*η*x2[i,j]
                                               + λ4[i,j] + μ21[i,j] - μ22[i,j] == 0)


# - Continuity condition on state variables
@NLconstraint(m,g13[i in 2:N],x1[i-1,M] == x1[i,1])
@NLconstraint(m,g14[i in 2:N],x2[i-1,M] == x2[i,1])
@NLconstraint(m,g15[i in 2:N],x3[i-1,M] == x3[i,1])
@NLconstraint(m,g16[i in 2:N],x4[i-1,M] == x4[i,1])
@NLconstraint(m,g17[i in 2:N],x5[i-1,M] == x5[i,1])
@NLconstraint(m,g18[i in 2:N],λ1[i-1,M] == λ1[i,1])
@NLconstraint(m,g19[i in 2:N],λ2[i-1,M] == λ2[i,1])
@NLconstraint(m,g20[i in 2:N],λ3[i-1,M] == λ3[i,1])
@NLconstraint(m,g21[i in 2:N],λ4[i-1,M] == λ4[i,1])
@NLconstraint(m,g22[i in 2:N],λ5[i-1,M] == λ5[i,1])


# - Expressions for u1 and u2
@NLconstraint(m,gu1[i in ni,j in 2:(M-1)],u1[i,j] == u1[i,1]
                                                    + (u1[i,M]-u1[i,1])*zc[j])

@NLconstraint(m,gu2[i in ni,j in mj;j!=2],u2[i,j] == u2[i,2])

# - Boundary values
@NLconstraint(m,g25,x1[1,1] == x1in)
@NLconstraint(m,g26,x2[1,1] == x2in)
@NLconstraint(m,g27,x3[1,1] == x3in)
@NLconstraint(m,g28,x4[1,1] == x4in)
@NLconstraint(m,g29,x5[1,1] == x5in)
@NLconstraint(m,g30,x5[N,M] == x5f)
@NLconstraint(m,g31,λ1[N,M] == λ1f)
@NLconstraint(m,g32,λ2[N,M] == λ2f)
@NLconstraint(m,g33,λ3[N,M] == η1)
@NLconstraint(m,g34,λ4[N,M] == η2)

# - Constraints of final time for some x
@NLconstraint(m,g35,x3[N,M] <= 45)
@NLconstraint(m,g36,x4[N,M] <= 135)

# - Transversality condition on H(tf)
@NLconstraint(m,Hf,Ham[N,M]  -ζ*x1[N,M]*log(x1[N,M]/x2[N,M]) 
                     - ϕ*x1[N,M]*u2[N,M]  + u1[N,M]*η1 + u2[N,M]+η2 == 0)

# - Continuity of H(t)
@NLconstraint(m,Hi1[i in 2:N],Ham[i,1] == Ham[i-1,M])


# Solution
optimize!(m)

# Reading results
x1s = transpose(convert(Array,value.(x1[:,:])))[:]
x2s = transpose(convert(Array,value.(x2[:,:])))[:]
x3s = transpose(convert(Array,value.(x3[:,:])))[:]
x4s = transpose(convert(Array,value.(x4[:,:])))[:]
x5s = transpose(convert(Array,value.(x5[:,:])))[:]
u1s = transpose(convert(Array,value.(u1[:,:])))[:]
u2s = transpose(convert(Array,value.(u2[:,:])))[:]
Hs  = transpose(convert(Array,value.(Ham[:,:])))[:]
u1s = transpose(convert(Array,value.(u1[:,:])))[:]
u2s = transpose(convert(Array,value.(u2[:,:])))[:]
μ11s = transpose(convert(Array,value.(μ11[:,:])))[:]   
μ12s = transpose(convert(Array,value.(μ12[:,:])))[:]  
μ21s = transpose(convert(Array,value.(μ21[:,:])))[:]   
μ22s = transpose(convert(Array,value.(μ22[:,:])))[:]  
λ1s = transpose(convert(Array,value.(λ1[:,:])))[:]  
λ2s = transpose(convert(Array,value.(λ2[:,:])))[:] 
λ3s = transpose(convert(Array,value.(λ3[:,:])))[:]
λ4s = transpose(convert(Array,value.(λ4[:,:])))[:]
λ5s = transpose(convert(Array,value.(λ5[:,:])))[:]  
η1s = value(η1)
η2s = value(η2)
hs = convert(Array,value.(h[:]))

# Plotting the results
include("plots.jl")


