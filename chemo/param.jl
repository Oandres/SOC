  
# Size parameters
N = 30    # Number of finite elements
M = 7     # Number of collocation points (including both ends)

# Sets
ni = 1:N;
mj = 1:M;
mk = copy(mj);
mj2 = 2:M

# Inner points for spline interp
p = 4

# Penalty parameter
ρ    = 100 

# Problem data
x1in = 9000   # Initial value for x1
x2in = 6000   # Initial value for x2
x3in = 0      # Initial value for x3
x4in = 0      # Initial value for x4
x5in = 0      # Initial value for x5 (t0)
x5f  = 7.51553# Final value for x5 (tf)
u1L  = 0      # Lower bound for u1
u1U  = 15     # Upper bound for u1
u2L  = 0      # Lower bound for u2
u2U  = 20     # Upper bound for u2
# - model parameters
ζ = 0.192
ϕ = 0.01
μ = 0.02
γ = 0.15
η = 0.025
b = 5.85
d = 0.00873

# Transversality conditions for the adjoint variables
λ1f  = 1
λ2f  = 0

# Collocation matrices
include("matrix.jl")       # It provides the collocation matrx and zeros
A,zc = colmatrix(M,"Radau");

# Weights associated with Radau quadrature
include("tableau.jl")      
Tab,ws,rs = butcher(M,"Radau");
w = Array{Float64}(undef,M)
w[2:M] = ws
